from comanche import *

### SCOPE
n_past = 36*12
n_future = 24*12

### LOGIC
f1 = lambda past: past['High-lin'][-1] - past['High'][-1]
f2 = lambda past: past['Mid-lin'][-1] - past['Mid'][-1]
f3 = lambda past: past['Low-lin'][-1] - past['Low'][-1]
f4 = lambda past: past.energy()
f5 = lambda past: past.swing()

loglist = [f1, f2, f3, f4, f5]
LOG = Logic(loglist)

### CRITERION
CRIT = Criterion(0.002, 0.001, 0.005)


### UI ENDS HERE #####################################################################################


### EXECUTION
A = Instrument('EURPLN=X')
A.download_history(8*w1, '5m', 'EURPLN')
EURPLN = A.datasets['EURPLN']

hi = max(EURPLN['High'])
lo = min(EURPLN['Low'])
mid = 0.5 * (hi + lo)

CRIT.get_prices(EURPLN)
EURPLN["Mid"] = 0.5 * (np.array(EURPLN['High']) + np.array(EURPLN['Low']))
EURPLN.normalize(['Mid'], high = hi, low = mid)

#A.datasets['EURPLN-PD'] = EURPLN.periodic_decomposition('High', [m5, m30, h1])
#print(A.datasets['EURPLN'])

samples = []
index = EURPLN.index
n_samp = len(index)

for i in range(n_past + 1, n_samp - n_future - 1, 10):

    ind = EURPLN.index[i]
    samp = Sample(EURPLN, ind, n_past, n_future)
    samp.past.linear_fit('High')
    samp.past.linear_fit('Mid')
    samp.past.linear_fit('Low')
    samp.get_rating(CRIT)
    samp.get_input(LOG)
    samples.append(samp)
    print(i)

agent = Agent()

agent.train(samples, epochs = 500)

B = Instrument("EURUSD=X")
B.download_history(8*w1, '5m', 'EURUSD')
EURUSD = B.datasets['EURUSD']

hi = max(EURUSD['High'])
lo = min(EURUSD['Low'])
mid = 0.5 * (hi + lo)

CRIT.get_prices(EURUSD)
EURUSD["Mid"] = 0.5 * (np.array(EURUSD['High']) + np.array(EURUSD['Low']))
EURUSD.normalize(['Mid'], high = hi, low = mid)

samples = []
index = EURUSD.index
n_samp = len(index)

for i in range(n_past + 1, n_samp - n_future - 1, 10):

    ind = EURUSD.index[i]
    samp = Sample(EURUSD, ind, n_past, n_future)
    samp.past.linear_fit('High')
    samp.past.linear_fit('Mid')
    samp.past.linear_fit('Low')
    samp.get_rating(CRIT)
    samp.get_input(LOG)
    samples.append(samp)
    print(i)

agent.evaluate(samples)


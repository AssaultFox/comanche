from datetime import timedelta
import datetime as dt
from re import L
from matplotlib.pyplot import hist
from matplotlib import pyplot as plt
from numpy.core.shape_base import atleast_1d
import yfinance as yf
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit as cf
import plotly.graph_objects as go
import plotly.express as px
import pytz 
import tensorflow as tf

### ### CONSTANTS

### TIME
utc=pytz.UTC
m1 = timedelta(minutes = 1)
m5 = timedelta(minutes = 5)
m15 = timedelta(minutes = 15)
m30 = timedelta(minutes = 30)
h1 = timedelta(minutes = 60)
h6 = timedelta(minutes = 6 * 60)
h12 = timedelta(minutes = 12 * 60)
d1 = timedelta(days = 1)
w1 = timedelta(days = 7)
w2 = timedelta(days = 14)
mo1 = timedelta(days = 30)
mo3 = timedelta(days = 90)
mo6 = timedelta(days = 180)
y1 = timedelta(days = 365)

timedict = {
    '1m' : m1,
    '5m' : m5,
    '15m': m15,
    '30m': m30,
    '1h' : h1,
    '6h' : h6,
    '12h': h12,
    '1d' : d1,
    '1w' : w1,
    '2w' : w2,
    '1mo': mo1,
    '3mo': mo3,
    '6mo': mo6,
    '1y' : y1,
    }


### FOREX SYMBOLS
USDPLN = 'PLN=X'
EURPLN = 'EURPLN=X'
AUDPLN = 'AUDPLN=X'
CADPLN = 'CADPLN=X'
GBPPLN = 'GBPPLN=X'

GBPUSD = 'GBPUSD=X'
EURUSD = 'EURUSD=X'
USDAUD = 'USDAUD=X'
USDCAD = 'CAD=X'

GBPEUR = 'GBPEUR=X'
GBPAUD = 'GBPAUD=X'
GBPCAD = 'GBPCAD=X'

Symbols_FX = [USDPLN, EURPLN, AUDPLN, CADPLN, GBPPLN, GBPUSD, EURUSD, USDAUD, USDCAD, GBPEUR, 
                GBPAUD, GBPCAD]


### CRYPTO SYMBOLS
BTC = 'BTC-USD'
ETH = 'ETH-USD'
RIP = 'XRP-USD'

Symbols_CRT = [BTC, ETH, RIP]


### ### TIME FUNCTIONS

def ago(delta):

    return dt.datetime.now() - delta


def now():
    return dt.datetime.now()


def delta_name(delta):

    m_in_h = 60
    m_in_d = 24 * m_in_h
    m_in_w = 7 * m_in_d
    m_in_mo = 30 * m_in_d

    minutes = delta.days * 24 * 60 + int(delta.seconds / 60)

    string = ""

    if minutes >= m_in_mo:

        string = string + "MO" + str(int(minutes/m_in_mo))
        minutes = minutes % m_in_mo

        if minutes != 0:
            string = string + "-"

    if minutes >= m_in_w:

        string = string + "W" + str(int(minutes/m_in_w))
        minutes = minutes % m_in_w

        if minutes != 0:
            string = string + "-"

    if minutes >= m_in_d:

        string = string + "D" + str(int(minutes/m_in_d))
        minutes = minutes % m_in_d

        if minutes != 0:
            string = string + "-"

    if minutes >= m_in_h:

        string = string + "H" + str(int(minutes/m_in_h))
        minutes = minutes % m_in_h

        if minutes != 0:
            string = string + "-"

    if minutes > 0:
        string = string + "M" + str(minutes)

    return string 


### ### AUX FUNCTIONS

def linear(x, A, B):

    return x * A + B


### ### CLASSES

### DataSetBase | base for DataSet to enable self replication
class DataSetBase:

    def trim_period(self, period = None):

        if period == None:
            return self
        
        elif isinstance(period, timedelta):
            index_0 = self.index[-int(period/self.delta)]
            index_f = self.index[-1]

        elif isinstance(period, int):
            index_0 = self.index[-period]
            index_f = self.index[-1]

        df = self.loc[index_0 : index_f, :]
        return DataSet(df)


    def periodic_decomposition(self, column, periods = [h1, d1, w1]):

        # Declare empty arrays
        raw = []
        filtered = []
        components = []

        # Fill the raw array, copying input data
        for index in self.index:

            raw.append(self.at[index, column])

        np.array(raw)

        # Fill the array of filtered data, using moving average
        for period in periods:
            
            A = self.moving_average(period, column = column, append = False)
            filtered.append(A)

        # Close the filtered array with average value of the input dataset
        avg = np.average(raw)
        filtered.append(len(raw) * [avg])

        # Fill the array of components, starting with raw data, completing with components
        components.append(raw)
        components.append(raw - filtered[0])

        # Generate components
        for i in range(0, len(filtered) - 1):
            components.append(filtered[i] - filtered[i+1])

        components.append(filtered[-1])

        # Generate dataframe
        #TODO nadawanie labelek i automatyczne ustawianie
        df = pd.DataFrame(index = self.index)
        df['Raw'] = raw
        df['Rest'] = components[1]

        for i in range(0, len(periods)):
            df[delta_name(periods[i])] = components[i+2]

        df['Avg'] = components[-1]

        return DataSet(df)
        

### DataSet | wrapper for pd.DataFrame, with added transformative functions for columns
class DataSet(pd.DataFrame, DataSetBase):

    @property
    def delta(self, n_max = 40):

        if len(self.index) < n_max:

            end = len(self.index)
        
        else:

            end = n_max

        for i in range(3, end):

            d1 = self.index[i] - self.index[i-1]
            d2 = self.index[i-1] - self.index[i-2]
            d3 = self.index[i-2] - self.index[i-3]

            if d1 == d2 and d2 == d3:
                return d1

        raise Exception('DataSet.delta: ambiguous interval.')


    @property
    def interval(self):

        return self.delta.days * 24 * 60 + self.delta.seconds / 60

    @property
    def length(self):

        return len(self.index)

    # OUTPUT FUNCTIONS

    def stats(self, column):

        return 0


    def plot(self, column):

        plt.plot(self[column])
        plt.show()

    
    # FUNCTIONALS

    def std_dev(self, period = None, column = None, type = 'avg'):

        if column == None:
            if not 'Avg' in self.columns:
                self.avg(label = 'Avg', append = True)
                col = 'Avg'
        
        else:
            col = column

        ds = self.trim_period(period)

        val = 0

        if type == 'avg':
            ref = ds.length * [np.average(ds[col])]

        elif type == 'lin':
            ref = ds.linear_fit(col, append = False)
            
        else:
            raise Exception('DataSet.std_dev: improper type specified.')

        for i in range(0, ds.length):

            val = val + (ds[col][i] - ref[i])**2

        return np.sqrt(val / ds.length)


    def lowest(self, period = None):

        ds = self.trim_period(period)

        return np.min(ds['Low'])


    def highest(self, period = None):

        ds = self.trim_period(period)

        return np.max(ds['High'])

    
    def swing(self, period = None):

        ds = self.trim_period(period)

        h = ds['High'].to_numpy()
        l = ds['Low'].to_numpy()
        s = h - l 
        a = np.average(s)

        return a


    def rel_swing(self, period = None):

        return 2 * self.swing(period) / (self['High'].iloc[-1] + self['Low'].iloc[-1])
    

    def energy(self, period = None):

        ds = self.trim_period(period)
        h = ds['High'].to_numpy()
        l = ds['Low'].to_numpy()
        s = h - l
        e = s * s
        ea = np.average(e)

        return np.sqrt(ea)
    

    def rel_energy(self, period = None):

        return 2 * self.energy(period) / (self['High'].iloc[-1] + self['Low'].iloc[-1])


    def average(self, period = None):

        ds = self.trim_period(period)

        if not 'Avg' in ds.columns:
            ds.avg(label = 'Avg', append = True)
        
        return np.average(ds['Avg'].to_numpy())


    def lintrend(self, period = None):

        ds = self.trim_period(period)

        if not 'Avg' in ds.columns:
            ds.avg(label = 'Avg', append = True)

        x = np.linspace(0, ds.length, ds.length)
        y = ds['Avg']

        res = cf(linear, x, y, [0, 0])

        return res [0][0]


    def alpha(self, period = None, part = 0.3):

        ds = self.trim_period(period)

        if not 'Support' in ds.columns:
            ds.support(part = part, absolute = False, append = True)

        if not 'Resistance' in ds.columns:
            ds.resistance(part = part, absolute = False, append = True)

        if not 'Avg' in ds.columns:
            ds.avg(append = True)

        return (self['Avg'][-1] - self['Support'][-1]) / (self['Resistance'][-1] - self['Support'][-1])


    # OPERATORS - CREATE A NEW COLUMN

    def differential(self, columns):
        None



    def normalize(self, columns, low = 0, high = 10):

        ran = high - low
        for column in columns:
            new = []

            for i in range(0, self.length):
                val = (self[column][i] - low) / ran
                new.append(val)
            
            self[column + '-norm'] = new

        

    def linear_fit(self, column):

        x = np.linspace(0, self.length, self.length)
        y = self[column]

        res = cf(linear, x, y, [0, 0])

        fun = lambda x: res[0][0] * x + res[0][1]
        y_lin = fun(x)

        self[column + '-lin'] = y_lin

        return [res[0][0], res[0][1]]


    #TODO: Dodać możliwość wyboru labelki dla poniższych
    def resistance(self, n = None, part = None, absolute = False, append = True):

        if n != None:
            if part != None:
                raise Exception('DataSet.support: ambiguous range specification.')

            else:
                n_active = n

        elif part != None:
            n_active = int(self.length * part)

        else:
            n_active = int(self.length)
        #TODO: this is a shitty fix to a shitty problem
        if n_active < 5:
            n_active = 5

        A = self['High'].to_numpy().copy()
        
        if absolute == False:
            if not 'Linear' in self.columns:
                if not 'Avg' in self.columns:
                    self.avg(label = 'Avg', append = True)
                
                self.linear_fit('Avg', append = True, label = 'Linear')

            A = A - self['Linear'].to_numpy()
        
        out = []
        
        x = []        
        y = []

        for i in range(0, n_active):
            
            arg = np.argmax(A)
            x.append(float(arg))
            y.append(self['High'][arg])
            A[arg] = -99999

        res = cf(linear, x, y, [0, 0])

        fun = lambda x: res[0][0] * x + res[0][1]


        for i in range(0, self.length):
            out.append(fun(i))

        if append == True:
            self['Resistance'] = out

        return out


    def support(self, n = None, part = None, absolute = False, append = True):

        if n != None:
            if part != None:
                raise Exception('DataSet.support: ambiguous range specification.')

            else:
                n_active = n

        elif part != None:
            n_active = int(self.length * part)

        else:
            n_active = int(self.length)

        #TODO: this is a shitty fix to a shitty problem
        if n_active < 5:
            n_active = 5
        A = self['Low'].to_numpy().copy()
        
        if absolute == False:
            if not 'Linear' in self.columns:
                if not 'Avg' in self.columns:
                    self.avg(label = 'Avg', append = True)
                
                self.linear_fit('Avg', append = True, label = 'Linear')

            A = A - self['Linear'].to_numpy()
        
        out = []
        
        x = []        
        y = []

        for i in range(0, n_active):
            
            arg = np.argmin(A)
            x.append(float(arg))
            y.append(self['Low'][arg])
            A[arg] = 99999

        res = cf(linear, x, y, [0, 0])

        fun = lambda x: res[0][0] * x + res[0][1]


        for i in range(0, self.length):
            out.append(fun(i))

        if append == True:
            self['Support'] = out

        return out





    #TODO: dodać opcję offsetowania
    #TODO: dodac uwzględnianie trendu liniowego zamiast średniej
    def read_peaks(self, label):

        avg = np.average(self[label])

        maxima = []
        minima = []

        maxima_labels = []
        minima_labels = []

        maxima_vals = []
        minima_vals = []

        max_so_far = 0
        min_so_far = 100

        i_max_so_far = 0
        i_min_so_far = 0

        rdy_max = True
        rdy_min = False

        for i in range(0, self.length):

            val = self[label][i]

            if rdy_max:
                if val > max_so_far:
                    max_so_far = val
                    i_max_so_far = i

                if val < avg:
                    maxima.append(i_max_so_far)
                    maxima_labels.append(self.index[i_max_so_far])
                    maxima_vals.append(max_so_far)
                    max_so_far = 0
                    rdy_max = False
                    rdy_min = True
                    
            if rdy_min:
                if val < min_so_far:
                    min_so_far = val
                    i_min_so_far = i

                if val > avg:
                    minima.append(i_min_so_far)
                    minima_labels.append(self.index[i_min_so_far])
                    minima_vals.append(min_so_far)
                    min_so_far = 100
                    rdy_max = True
                    rdy_min = False

        print (len(maxima_labels), len(maxima_vals), len(minima_labels), len(minima_vals))

        return [pd.DataFrame(data = maxima_vals, index = maxima_labels),
                pd.DataFrame(data = minima_vals, index = minima_labels)]


    #TODO: add kinds (rear, central, front)
    def moving_average(self, period, label = 'MA', column = 'Open', append = True):

        period_minutes = period.days * 24 * 60 + period.seconds / 60

        n = period_minutes / self.interval

        if n % 2 == 0:
            n = n + 1

        n_half = int((n - 1) / 2)

        out = []

        for i in range(0, n_half):

            val = 0

            for j in range(0, i + n_half):
                
                index = self.index[j]
                val = val + self.at[index, column]
            
            val = val / (i + n_half)
            out.append(val)
            
        for i in range(n_half, self.length - n_half):

            val = 0

            for j in range(-n_half, n_half + 1):

                index = self.index[i + j]
                val = val + self.at[index, column]

            val = val/n
            out.append(val)

        for i in range(self.length - n_half, self.length):

            val = 0

            for j in range(i - n_half, self.length):
                
                index = self.index[j]
                val = val + self.at[index, column]
            
            val = val / (self.length - i + n_half)
            out.append(val)

        if append:
            self[label] = out

        return np.array(out)


    def price(self, specification = 'auto', spread = 0, append = True):

        if specification == 'auto':
            ask = self['High']
            bid = self['Low']

        elif specification == 'spread':
            avg_arr = np.array(self.avg(label = 'Avg', append = False))
            ask = avg_arr + spread * 0.5
            bid = avg_arr - spread * 0.5

        if append == True:
            self['Ask'] = ask
            self['Bid'] = bid

        return [ask, bid]
            

    # RAW DOWNLOAD SPECIFIC OPERATORS
    def avg(self, label = 'Avg', append = True):

        out = []

        for index in self.index:

            high = self.at[index, 'High']
            low = self.at[index, 'Low']
            val = 0.5 * (high + low)
            out.append(val)

        if append:
            self[label] = out

        return out

### INSTRUMENT | handles and stores multiple datasets of market history and its indicators
class Instrument:

    def __init__(self, name, verbosity = 0):

        self.name = name
        self.verbosity = verbosity
        self.ticker = yf.Ticker(name)
        self.datasets = dict()

    #Downloads history into specified dataset
    def download_history(self, period, interval_, label=None):

        #Catch errors and handle problems with buggy history download in yfinance
        h = self.ticker.history(end = now() - m1, start = now() - period + m1, interval = interval_)

        if len(h.index) == 0:
            try: delta_time = timedict[interval_]
            except KeyError: raise Exception("Instrument.download_history(): invalid interval")
            h = self.ticker.history(end = now() - delta_time,
                                    start = now() - period, 
                                    interval = interval_)
            
            if len(h.index) == 0:
                try: delta_time = timedict[interval_]
                except KeyError: raise Exception("Instrument.download_history(): invalid interval")
                h = self.ticker.history(end = now() - delta_time,
                                        start = now() - period + delta_time, 
                                        interval = interval_)
                
                if len(h.index) == 0:
                    raise Exception("Instrument.download_history(): unknown error. Can't download history.")

        try:
            h = h.drop(columns = 'Dividends')
            h = h.drop(columns = 'Volume')
            h = h.drop(columns = 'Stock Splits')

        except KeyError:
            None

        finally:
            ds = DataSet(h)

        if label != None:
            self.datasets[label] = ds

        else:
            self.datasets[interval_] = ds

        if self.verbosity > 0:
            print("Succesfully downloaded history of " + self.name + " for " + interval_ + ' interval')


    #Loads history from a saved .npy file
    def load_history(self, label, filename):

        #Catch error if filename lacks suffix
        try: A = np.load(filename, allow_pickle = True)
        except: 
            try: A = np.load(filename + ".npy", allow_pickle = True)
            except: raise Exception('Instrument.load_history(): failed to load history')

        df = DataSet(index = A[0])
        df['Open'] = A[1]
        df['Close'] = A[2]
        df['Low'] = A[3]
        df['High'] = A[4]

        self.datasets[label] = df

        if self.verbosity > 0:
            print("Successfully loaded history of " + self.name + " from " + filename)

    #Saves history to a .npy file
    def save_history(self, label, filename):

        A0 = self.datasets[label].index
        A1 = self.datasets[label]['Open']
        A2 = self.datasets[label]['Close']
        A3 = self.datasets[label]['Low']
        A4 = self.datasets[label]['High']

        np.save(filename, [A0, A1, A2, A3, A4], allow_pickle = True)

        if self.verbosity > 0:
            print('Successfully saved history of ' + self.name + ' at ' + filename + '.npy')


    #Appends to saved history in .npy file
    def append_history(self, label, filename):

        #Check if target file exists
        try:
            self.load_history('temp', filename)
        except:
            raise Exception('Instrument.append_history(): can not load history file.')

        #Check if target file is compatible with currently available data (not too old)
        #First check if files are of the same time interval
        interval_old = self.datasets['temp'].index[1] - self.datasets['temp'].index[0]
        interval_new = self.datasets[label].index[1] - self.datasets[label].index[0]
        err = (interval_old - interval_new)/interval_old
        
        if abs(err) > 0.2:
            raise Exception('Instrument.append_history(): intervals of concatenated sets are not the same')
        
        #Then check if there is no gap between the two periods
        if self.datasets['temp'].index[-1] < self.datasets[label].index[0]:
            raise Exception('Instrument.append_history(): gap in concatenated datasets')
        
        #If ok, find the index of currently stored data that marks the end of data overlap
        for i in range(0, len(self.datasets[label].index)):
            if self.datasets[label].index[i] > self.datasets['temp'].index[-1]:
                #Then slice the stored data to begin with that index and append to temp
                #Then, terminate loop
                self.datasets['temp'] = pd.concat((self.datasets['temp'], self.datasets[label][i:]))
                break
        self.save_history('temp', filename)


    def plot(self, label, columns = None):

        dataset = self.datasets[label]
        if columns == None:
            candlestick = go.Figure(data = [go.Candlestick(x = dataset.index, 
                                               open = dataset['Open'], 
                                               high = dataset['High'], 
                                               low = dataset['Low'], 
                                               close = dataset['Close'])])

            candlestick.update_layout(xaxis_rangeslider_visible = False, title = 'Instrument price: ' + self.name)
            candlestick.update_xaxes(title_text = 'Date')

            candlestick.show()

        else:
            fig = px.line(dataset, x=dataset.index, y=columns)
            fig.show()


### AI PART ##################

### CRITERION | defines how to interpret market properties and investing preferences
class Criterion:

    def __init__(self, spread, gain, max_risk=0.):

        self.spread = spread
        self.gain = gain
        self.max_risk = max_risk


    def __call__(self, sample):
        
        warn_flag_long = False
        warn_flag_short = False
        win_flag_long = False
        win_flag_short = False

        future = sample.future[:]
        start = future.index[0]
        long_open = 0.5 * (future.at[start, 'Open'] + future.at[start, 'Close']) + 0.5 * self.spread
        short_open = long_open - self.spread

        long_max_profit = 0
        long_min_profit = 0
        long_max_risk = 0

        short_max_profit = 0
        short_min_profit = 0
        short_max_risk = 0

        for ind in future.index:

            short_close =  0.5 * (future.at[ind, 'Open'] + future.at[ind, 'Close']) + 0.5 * self.spread
            long_close = short_close - self.spread
            
            long_profit = long_close - long_open
            short_profit = short_open - short_close

            long_rel_profit = long_profit / long_open
            short_rel_profit = short_profit / short_open

            if (long_rel_profit > long_max_profit): long_max_profit = long_rel_profit
            elif (long_rel_profit < long_max_risk): long_max_risk = long_rel_profit

            if (short_rel_profit > short_max_profit): short_max_profit = short_rel_profit
            elif (short_rel_profit < short_max_risk): short_max_risk = short_rel_profit

        
        long_crit = long_rel_profit > self.gain and abs(long_max_risk) < abs(self.max_risk)
        short_crit = short_rel_profit > self.gain and abs(short_max_risk) < abs(self.max_risk)

        if long_crit and not short_crit: return 1
        
        elif short_crit and not long_crit: return 2
        
        elif not short_crit and not long_crit: return 0
        
        elif long_crit and short_crit:
            long_power = (long_profit - self.gain) / (abs(long_max_risk) - abs(self.max_risk))
            short_power = (short_profit - self.gain) / (abs(short_max_risk) - abs(self.max_risk))

            if long_power > short_power: return 1
            else: return 2


    def get_prices(self, dataset):
        
        bid = []
        ask = []

        for ind in dataset.index:
            avg = 0.5 * (dataset.at[ind, 'Open'] + dataset.at[ind, 'Close'])
            bid.append(avg - 0.5 * self.spread)
            ask.append(avg + 0.5 * self.spread)

        dataset['Ask']=ask
        dataset['Bid']=bid

### LOGIC | defines how to transform market history into input for samples
class Logic:

    def __init__(self, logic_list):
        
        self.logic_list = logic_list
        self.size = 0

    def __call__(self, past):

        input = []

        for item in self.logic_list:
            
            if isinstance(item, list):
                first = item[0]
                second = item[1]

                if isinstance(first, int) or isinstance (first, timedelta) or first == None:
                    cut = past.trim_period(first)

                    for entry in cut[second]:
                        input.append(entry)

            elif callable(item):
                input.append(item(past))
            
        return input

### SAMPLE | provides consolidated input data for the solver and investment decision (output)
class Sample:

    def __init__(self, dataset, point, n_past, n_future):

        if not isinstance(point, dt.datetime):
            raise Exception('make_sample: point must be datetime type.')

        if not (isinstance(n_past, int) and isinstance(n_future, int)):
            raise Exception('make_sample: n_past and n_future must be int type.')

        if not(isinstance(dataset, DataSet)):
            raise Exception('make_sample: dataset must be DataSet type.')

        n_point = list(dataset.index).index(point)
        index_past = dataset.index[n_point - n_past:n_point]
        index_future = dataset.index[n_point:n_point + n_future]
        past = dataset[dataset.index.isin(index_past)]
        future = dataset[dataset.index.isin(index_future)]

        if len(past) < n_past or len (future) < n_future:
            raise Exception('Sample.__init__: incorrect point placement. Too close to edge?')

        self.point = point
        self.past = DataSet(past)
        self.future = DataSet(future)
        self.rating = None
        self.dataset = dataset


    def get_rating(self, crit):

        self.rating = crit(self)


    def export_data(self):

        if self.rating == None:
            raise Exception("Sample.export_data: no rating to export")
        
        return self.rating


    def plot(self, crit):

        dataset_cp = self.dataset.copy()
        crit.get_prices(dataset_cp)
        cols=[dataset_cp['Ask'], dataset_cp['Bid']]
        fig = px.line(dataset_cp, x=dataset_cp.index, y=cols)
        fig.show()


    def get_input(self, logic):
        
        self.input = logic(self.past)


### AGENT | wrapper for TF MLP, learns from samples to gain predictive capability
class Agent:

    def __init__(self):

        self.past_size = None
        self.model = None


    def __call__(self, input):

        res = self.model.predict(np.array([input]))
        
        for i in range(0, len(res[0])):
            if max(res[0]) == res[0][i]: return i

    #TODO: fix in accordance to _call_ now accepting input instead of dataset
    def run(self, dataset, criterion):

        in_long = False
        in_short = False
        in_at = 0
        mul = 1

        act = 0

        for i in range(self.past_size, len(dataset.index)):

            past = dataset[dataset.index.isin(dataset.index[i-self.past_size:i])]
            ok = []
            for column in self.source:
                ok.append(list(past[column]))

            if i%12 == 0:
                print(i/12, ' hours in...')

            res = self.model.predict(np.array([ok]), verbose = 0)
            
            for j in range(0, len(res[0])):
                if max(res[0]) == res[0][j]: act = j


            # Place bets
            if act == 1 and not in_long and not in_short:
                in_long = True
                in_at = dataset["Ask"][i]
                print("Opened LONG at ", in_at)

            elif act == 2 and not in_long and not in_short:
                in_short = True
                in_at = dataset["Bid"][i]
                print("Opened SHORT at ", in_at)

            # Collect
            if in_long and not in_short and dataset["Bid"][i] >= (criterion.gain + 1) * in_at:
                in_long = False
                mul = mul * dataset["Bid"][i] / in_at
                print("Closed LONG (COLLECT) at ", dataset["Bid"][i], "\tScore: ", mul)


            if in_short and not in_long and dataset["Ask"][i] <= in_at / (criterion.gain + 1):
                in_short = False
                mul = mul * in_at / dataset["Ask"][i]
                print("Closed SHORT (COLLECT) at ", dataset["Ask"][i], "\tScore: ", mul)

            # Stop Loss
            if in_long and not in_short and dataset["Bid"][i] <= in_at / (1 - criterion.max_risk):
                in_long = False
                mul = mul * dataset["Bid"][i] / in_at
                print("Closed LONG (STOP LOSS) at ", dataset["Bid"][i], "\tScore: ", mul)

            if in_short and not in_long and dataset["Ask"][i] >= in_at * (criterion.max_risk + 1):
                in_short = False
                mul = mul * in_at / dataset["Ask"][i]
                print("Closed SHORT (STOP LOSS) at ", dataset["Bid"][i], "\tScore: ", mul)


    def train(self, samples, epochs = 300):

        sample_index_length = len(samples[0].input)
        self.past_size = sample_index_length

        for sample in samples:

            if len(sample.input) != sample_index_length:
                raise Exception('Agent.train: inconsistent sample past size')

            if sample.rating == None:
                raise Exception('Agent.train: unrated sample detected')

        self.model = tf.keras.Sequential([
        tf.keras.layers.Dense(sample_index_length),
        tf.keras.layers.Dense(300, activation='sigmoid'),
        tf.keras.layers.DepthwiseConv1D(100, activation='sigmoid'),
        tf.keras.layers.Dense(10, activation='relu'),
        tf.keras.layers.Dense(100, activation='relu'),
        tf.keras.layers.Dense(3)
        ])

        self.model.compile(optimizer='adam',
            loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            metrics=['accuracy'])

        train_data=[]
        train_ratings=[]

        for sample in samples:
            train_data.append(sample.input)
            train_ratings.append(sample.rating)

        train_data=np.array(train_data)
        train_ratings=np.array(train_ratings)


        print(len(train_data))
        self.model.fit(train_data, train_ratings, epochs=epochs)


    def evaluate(self, samples):

        eval_data=[]
        eval_ratings=[]

        for sample in samples:
            eval_data.append(sample.input)
            eval_ratings.append(sample.rating)

        eval_data=np.array(eval_data)
        eval_ratings=np.array(eval_ratings)

        self.model.evaluate(eval_data, eval_ratings, verbose=2)

